import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as disp

import numpy as np
import matplotlib.pyplot as plt

class TestCpuGpu():

    def setup_class(self):
        # couple different image spacings
        self.imSpacingRegular = ca.Vec3Df(1,1,1)
        self.imSpacingAnisotropic = ca.Vec3Df(1.8,2.1,1.7)
        self.allspaces = [self.imSpacingRegular, self.imSpacingAnisotropic]

        # few different sizes
        self.imSizeSquare = ca.Vec3Di(32,32,32)
        self.imSizeSquareBig = ca.Vec3Di(128,128,128)
        self.imSizeRectangle = ca.Vec3Di(30,51,42)
        self.imsizes = [self.imSizeSquare, self.imSizeSquareBig,
                self.imSizeRectangle]

        # sizes for slices too
        self.sliceSizeSquare = ca.Vec3Di(32,32,1)
        self.sliceSizeSquareBig = ca.Vec3Di(128,128,1)
        self.sliceSizeRectangle = ca.Vec3Di(30,51,1)
        self.slicesizes = [self.sliceSizeSquare, self.sliceSizeSquareBig,
                self.sliceSizeRectangle]

        # a few origins
        self.origZero = ca.Vec3Df(0,0,0)
        self.origOffsetX = ca.Vec3Df(5.1,0,0)
        self.origOffsetY = ca.Vec3Df(0,12.3,0)
        self.origOffsetXY = ca.Vec3Df(5.1,12.3,0)
        self.origOffsetXYZ = ca.Vec3Df(5.1,12.3,7.9)
        self.allorigins = [self.origZero, self.origOffsetX, self.origOffsetY,
                self.origOffsetXY, self.origOffsetXYZ]

    def rand_image(self, grid, mType):
        """
        Generate a random image of a certain shape and memory type
        """
        return common.RandImage(self.Vec3toNP(grid.size()), nSig=1.0, gSig=0.0, 
                             mType = mType, sp = grid.spacing(), orig =
                             grid.origin())

    def rand_field(self, grid, mType):
        h = common.RandField(self.Vec3toNP(grid.size()), nSig=1.0, gSig=0.0, 
                             mType = mType, sp = grid.spacing(), orig =
                             grid.origin())
        ca.VtoH_I(h)
        return h

    def Vec3toNP(self, v):
        return (v.x, v.y, v.z)


    def test_SplatWorldOnes(self):
        """
        Splatting a ones image should give us exactly the weights image
        """
        for imsz in self.imsizes:
            for imsp in self.allspaces:
                for imor in self.allorigins:
                    imgrid = ca.GridInfo(imsz, imsp, imor)
                    I = ca.Image3D(imgrid, ca.MEM_HOST)
                    # holds weights
                    w = ca.Image3D(imgrid, ca.MEM_HOST)
                    for slsz in self.slicesizes+self.imsizes:
                        for slsp in self.allspaces:
                            for slor in self.allorigins:
                                slgrid = ca.GridInfo(slsz, slsp, slor)

                                sl = ca.Image3D(slgrid, ca.MEM_HOST)
                                ca.SetMem(sl, 1.0)

                                ca.SetMem(I, 0)
                                ca.SetMem(w, 0)

                                ca.SplatWorld(I, sl, w)

                                yield self.images_match, [I], [w], 1e-9, True


    def test_SplatWorldWeightsNoWeights(self):
        for imsz in self.imsizes:
            for imsp in self.allspaces:
                for imor in self.allorigins:
                    imgrid = ca.GridInfo(imsz, imsp, imor)
                    I = ca.Image3D(imgrid, ca.MEM_HOST)
                    Iw = ca.Image3D(imgrid, ca.MEM_HOST)
                    # holds weights
                    w = ca.Image3D(imgrid, ca.MEM_HOST)
                    for slsz in self.slicesizes+self.imsizes:
                        for slsp in self.allspaces:
                            for slor in self.allorigins:
                                slgrid = ca.GridInfo(slsz, slsp, slor)

                                sl = self.rand_image(slgrid, ca.MEM_HOST)

                                ca.SetMem(I, 0)
                                ca.SetMem(Iw, 0)
                                ca.SetMem(w, 0)

                                ca.SplatWorld(I, sl)
                                ca.SplatWorld(Iw, sl, w)

                                yield self.images_match, [I], [Iw], 1e-9, True


    def images_match(self, aa, bb, tol, expected):
        for (ii, (a,b)) in enumerate(zip(aa,bb)):
            print "Comparing image pair {} of {}".format(ii, len(aa))
            # compute L2 of each image
            l2a = ca.Sum2(a)
            l2b = ca.Sum2(b)

            # compute L2 of difference
            t = ca.Image3D(b.grid(), b.memType())
            ca.Copy(t, a)
            ca.Sub_I(t, b)
            l2diff = ca.Sum2(t)

            # compute relative error
            match = l2diff < 0.5 * (l2a+l2b) * tol

            if match != expected:
                print "l2a={} l2b={} l2diff={} tol={}".format(l2a, l2b, l2diff, tol)

                # choose slice to output
                #sl = a.size().z-1
                sl = a.size().z // 2
                #sl = 0
                print a
                print b

                if b.memType() == ca.MEM_DEVICE:
                    arr = ca.Image3D(b.grid(), ca.MEM_HOST)
                    ca.Copy(arr, t)
                    print np.argwhere(arr.asnp())
                else:
                    print np.argwhere(t.asnp())

                plt.figure('error')
                plt.clf()
                plt.subplot(1,3,1)
                disp.DispImage(a, newFig=False, sliceIdx=sl)
                plt.colorbar()
                plt.subplot(1,3,2)
                disp.DispImage(t, newFig=False, sliceIdx=sl)
                plt.colorbar()
                plt.subplot(1,3,3)
                disp.DispImage(b, newFig=False, sliceIdx=sl)
                plt.colorbar()
                plt.savefig('error.{}.png'.format(l2diff))

            assert match == expected
