#
# This file contains testing of the Gaussian Filter.
# All tests can be run from the command line by:
#
# > python -m unittest discover -v -p '*UnitTest.py'
#
# To run an individual test with graphical output from ipython:
#
# import GaussianFilterUnitTest as cgtest
# cgtc = cgtest.GaussianFilterTestCase()
# cgtc.test_GaussianFilter()
#
import unittest

import PyCA.Core as ca
import numpy as np

# Test Class
#
class GaussianFilterTestCase(unittest.TestCase):

    def __init__(self, methodName='runTest'):
        super(GaussianFilterTestCase, self).__init__(methodName)

        self.cudaEnabled = (ca.GetNumberOfCUDADevices() > 0)

        # grid for 2d image
        self.grid2D = ca.GridInfo(ca.Vec3Di(100, 128, 1),
                                  ca.Vec3Df(1.5, 2.5, 1),
                                  ca.Vec3Df(-50, -60, 0))

        # grid for 3d image
        self.grid3D = ca.GridInfo(ca.Vec3Di(20, 24, 28),
                                  ca.Vec3Df(1.5, 2.5, 3.5),
                                  ca.Vec3Df(-10, -20, -30))


    def skipIfNoCUDA(self):
        if not self.cudaEnabled:
            self.skipTest('Cannot run test, no CUDA device found or CUDA ' +
                          'support not compiled')

    ################################################################
    #
    # Begin Tests
    #
    ################################################################
    def test_GaussianFilter(self):
        mType = ca.MEM_DEVICE
        grid2D = self.grid2D
        grid3D = self.grid3D
        Im2D = ca.Image3D(grid2D, mType)
        Im3D = ca.Image3D(grid3D, mType)
        Im2Dfilt = ca.Image3D(grid2D, mType)
        Im3Dfilt = ca.Image3D(grid3D, mType)
        Im2Dtmp = ca.Image3D(grid2D, mType)
        Im3Dtmp = ca.Image3D(grid3D, mType)
        ca.SetMem(Im2D, 4.0)
        ca.SetMem(Im3D, 8.0)

        g2D = ca.GaussianFilterGPU()
        g2D.updateParams(grid2D.size(), ca.Vec3Df(1.0, 1.0, 1.0), ca.Vec3Di(2, 2, 0))
        g3D = ca.GaussianFilterGPU()
        g3D.updateParams(grid3D.size(), ca.Vec3Df(1.0, 1.0, 1.0), ca.Vec3Di(2, 2, 2))

        g2D.filter(Im2Dfilt, Im2D, Im2Dtmp)
        g3D.filter(Im3Dfilt, Im3D, Im3Dtmp)

        # print ca.MinMax(Im2D)
        # print ca.MinMax(Im2Dfilt)
        # print ca.MinMax(Im3D)
        # print ca.MinMax(Im3Dfilt)

        self.assertAlmostEqual(ca.Min(Im2Dfilt), ca.Min(Im2D), 5)  # FAILS
        self.assertAlmostEqual(ca.Max(Im2Dfilt), ca.Max(Im2D), 5)  # FAILS
        self.assertAlmostEqual(ca.Min(Im3Dfilt), ca.Min(Im3D), 5)
        self.assertAlmostEqual(ca.Max(Im3Dfilt), ca.Max(Im3D), 5)

        # Do the exact same for CPU
        mType = ca.MEM_HOST
        grid2D = self.grid2D
        grid3D = self.grid3D
        Im2D = ca.Image3D(grid2D, mType)
        Im3D = ca.Image3D(grid3D, mType)
        Im2Dfilt = ca.Image3D(grid2D, mType)
        Im3Dfilt = ca.Image3D(grid3D, mType)
        Im2Dtmp = ca.Image3D(grid2D, mType)
        Im3Dtmp = ca.Image3D(grid3D, mType)
        ca.SetMem(Im2D, 2.0)
        ca.SetMem(Im3D, 5.0)

        g2D = ca.GaussianFilterCPU()
        g2D.updateParams(grid2D.size(), ca.Vec3Df(1.0, 1.0, 1.0), ca.Vec3Di(2, 2, 0))
        g3D = ca.GaussianFilterCPU()
        g3D.updateParams(grid3D.size(), ca.Vec3Df(1.0, 1.0, 1.0), ca.Vec3Di(2, 2, 2))

        g2D.filter(Im2Dfilt, Im2D, Im2Dtmp)
        g3D.filter(Im3Dfilt, Im3D, Im3Dtmp)

        # print ca.MinMax(Im2D)
        # print ca.MinMax(Im2Dfilt)
        # print ca.MinMax(Im3D)
        # print ca.MinMax(Im3Dfilt)

        self.assertAlmostEqual(ca.Min(Im2Dfilt), ca.Min(Im2D), 5)
        self.assertAlmostEqual(ca.Max(Im2Dfilt), ca.Max(Im2D), 5)
        self.assertAlmostEqual(ca.Min(Im3Dfilt), ca.Min(Im3D), 5)
        self.assertAlmostEqual(ca.Max(Im3Dfilt), ca.Max(Im3D), 5)


    # runTest is only added so that the class can be instantiated
    # directly in order to call individual tests
    def runTest():
        print 'No tests to run directly, all are member functions'
