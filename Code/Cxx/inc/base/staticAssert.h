/* ================================================================
 *
 * PyCA Project
 *
 * Copyright (c) J. Samuel Preston, Linh K. Ha, Sarang C. Joshi. All
 * rights reserved.  See Copyright.txt or for details.
 *
 * This software is distributed WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the above copyright notice for more information.
 *
 * ================================================================ */

#ifndef __STATIC_ASSERT_H
#define __STATIC_ASSERT_H

#ifdef _MSC_VER
#define STATIC_ASSERT assert
#else
#define STATIC_ASSERT(x)                        \
	typedef char StaticAssert[(x) ? 1 : -1];
#endif

#endif // __STATIC_ASSERT_H
