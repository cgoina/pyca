/* ================================================================
 *
 * PyCA Project
 *
 * Copyright (c) J. Samuel Preston, Linh K. Ha, Sarang C. Joshi. All
 * rights reserved.  See Copyright.txt or for details.
 *
 * This software is distributed WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the above copyright notice for more information.
 *
 * ================================================================ */

#ifndef __GGAUSS_UTILS_H
#define __GGAUSS_UTILS_H

#include <estream.h>

namespace PyCA {

namespace GGaussUtils {
    void ConvolutionX3D(float* d_o, const float* d_i, const float* h_kernel, const float* h_sKernel,
                        int kRadius, int sizeX, int sizeY, int sizeZ, StreamT stream);
    void ConvolutionY3D(float* d_o, const float* d_i, const float* h_kernel, const float* h_sKernel,
                        int kRadius, int sizeX, int sizeY, int sizeZ, StreamT stream);
    void ConvolutionZ3D(float* d_o, const float* d_i, const float* h_kernel, const float* h_sKernel,
                        int kRadius, int sizeX, int sizeY, int sizeZ, StreamT stream);

}

} // end namespace PyCA

#endif
