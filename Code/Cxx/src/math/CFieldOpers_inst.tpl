def IOperBG = [
BACKGROUND_STRATEGY_CLAMP,
BACKGROUND_STRATEGY_WRAP,
BACKGROUND_STRATEGY_ZERO,
BACKGROUND_STRATEGY_PARTIAL_ZERO]

def HOperBG = [
BACKGROUND_STRATEGY_ID,
BACKGROUND_STRATEGY_PARTIAL_ID,
BACKGROUND_STRATEGY_CLAMP]

def VOperBG = [
BACKGROUND_STRATEGY_ZERO,
BACKGROUND_STRATEGY_PARTIAL_ZERO,
BACKGROUND_STRATEGY_WRAP,
BACKGROUND_STRATEGY_CLAMP]

def FOperBG = [
BACKGROUND_STRATEGY_ZERO,
BACKGROUND_STRATEGY_PARTIAL_ZERO,
BACKGROUND_STRATEGY_ID,
BACKGROUND_STRATEGY_PARTIAL_ID,
BACKGROUND_STRATEGY_WRAP,
BACKGROUND_STRATEGY_CLAMP]

def FieldInterp = [
INTERP_NN,
INTERP_LINEAR]

def RescaleVector = [
true,
false]

template void CFieldOpers::
ComposeHH<${HOperBG}>(Field3D&, const Field3D&, const Field3D&, StreamT);

template void CFieldOpers::
ComposeVH<${VOperBG}>(Field3D&, const Field3D&, const Field3D&, const float&, StreamT,bool);

template void CFieldOpers::
ComposeVInvH<${VOperBG}>(Field3D&, const Field3D&, const Field3D&, const float&, StreamT,bool);

template void CFieldOpers::
ComposeHV<${HOperBG}>(Field3D&, const Field3D&, const Field3D&, const float&, StreamT,bool);

template void CFieldOpers::
ComposeHVInv<${HOperBG}>(Field3D&, const Field3D&, const Field3D&, const float&, StreamT,bool);

template void CFieldOpers::
ComposeTranslation<${FOperBG}>(Field3D&, const Field3D&, const Vec3Df&, StreamT,bool);

template void CFieldOpers::
ApplyH<${FOperBG}, ${FieldInterp}>(Field3D&, const Field3D&, const Field3D&, StreamT st);

template void CFieldOpers::
ApplyV<${FOperBG}>(Field3D&, const Field3D&, const Field3D&, const float&, StreamT s,bool);

template void CFieldOpers::
ApplyVInv<${FOperBG}>(Field3D&, const Field3D&, const Field3D&, const float&, StreamT s,bool);

template void CFieldOpers::
SplatField<BACKGROUND_STRATEGY_ZERO>(Field3D&, const Field3D&, const Field3D&, StreamT);

template void CFieldOpers::
Splat<${IOperBG}>(Field3D&, const Field3D&, const Field3D&, StreamT);

template void CFieldOpers::
Resample<${FOperBG}, ${RescaleVector}>(Field3D&, const Field3D&, StreamT);

template void CFieldOpers::
FixedPointInverse<${HOperBG}>(Field3D &ginv, const Field3D& g, unsigned int numIter, StreamT stream, bool onDev);

// #ifndef _MSC_VER
// // Fix MSVC complaining about a redeclaration here. Not sure why it is -JDH Nov 2013
// void CFieldOpers::UpdateInverse(Field3D &ginv0t1,Field3D &scratchV, const Field3D& ginv0t, const Field3D& w, unsigned int numIter, StreamT stream, bool onDev);
// #endif
template void CFieldOpers::
Laplacian<${IOperBG}>(Field3D&, const Field3D&, StreamT);

template void CFieldOpers::
Ad<${VOperBG}>(Field3D& Z, const Field3D& g, const Field3D& X, StreamT s,bool onDev);

template void CFieldOpers::
CoAd<${VOperBG}>(Field3D& n, const Field3D& g, const Field3D& m, StreamT s,bool onDev);
