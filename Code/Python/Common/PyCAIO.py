"""File IO methods"""

import PyCA.Core as core
import PyCACommon as common

import numpy as np
import os.path

try:
    from PIL import Image
    HAS_PIL = True
    PIL_EXT = ['.png', '.jpg', '.jpeg', '.gif', '.tif', '.tiff']
except:
    HAS_PIL = False
    PIL_EXT = []




def SaveImageNPY(Im, fname):
    """Save an image in *.npy format (no origin/spacing info)"""

    # removing .asnp() for now, at least until bug is tracked down
    # -jsp20150330
    # if Im.memType() == core.MEM_DEVICE:
    #     nparr = common.AsNPCopy(Im)
    # else:
    #     nparr = Im.asnp()

    nparr = common.AsNPCopy(Im)

    np.save(fname, nparr)


def LoadImageNPY(fname, mType=core.MEM_HOST):
    """Load an image in *.npy format (no origin/spacing info)"""
    npy = np.load(fname)
    I = common.ImFromNPArr(npy, mType=mType)
    return I

def SaveImageNPZ(Im, fname, useCompression=True):
    """Save an image in *.npz format (records origin/spacing info)"""
    origin = Im.grid().origin().tolist()
    spacing = Im.grid().spacing().tolist()

    # removing .asnp() for now, at least until bug is tracked down
    # -jsp20150330
    # if Im.memType() == core.MEM_DEVICE:
    #     nparr = common.AsNPCopy(Im)
    # else:
    #     nparr = Im.asnp()
    nparr = common.AsNPCopy(Im)

    if useCompression:
        np.savez_compressed(fname, data=nparr, origin=origin,
                spacing=spacing)
    else:
        np.savez(f, data=nparr, origin=origin, spacing=spacing)


def LoadImageNPZ(fname, mType=core.MEM_HOST):
    """Load an image in *.npz format (with proper origin/spacing info)"""
    # TODO: put some format validation in here
    npz = np.load(fname)
    spacing = core.Vec3Df()
    spacing.fromlist(npz['spacing'])
    origin = core.Vec3Df()
    origin.fromlist(npz['origin'])
    I = common.ImFromNPArr(npz['data'], mType=mType, sp=spacing, orig=origin)
    del npz.f
    npz.close()
    return I


def SaveFieldNPZ(v, fname, useCompression=True):
    """Save a field in *.npz format (records origin/spacing info)"""
    origin = v.grid().origin().tolist()
    spacing = v.grid().spacing().tolist()

    # removing .asnp() for now, at least until bug is tracked down
    # -jsp20150330
    # if v.memType() == core.MEM_DEVICE:
    #     nparr = common.AsNPCopy(v)
    # else:
    #     nparr = np.transpose(v.asnp(), (1,2,3,0))
    nparr = common.AsNPCopy(v)

    # we will save in X by Y by Z by 3 format
    if useCompression:
        np.savez_compressed(fname, data=nparr, origin=origin,
                spacing=spacing)
    else:
        np.savez(fname, data=nparr, origin=origin, spacing=spacing)


def LoadFieldNPZ(fname, mType=core.MEM_HOST):
    """Load a field in *.npz format (with proper origin/spacing info)"""
    # TODO: put some format validation in here
    npz = np.load(fname)
    spacing = core.Vec3Df()
    spacing.fromlist(npz['spacing'])
    origin = core.Vec3Df()
    origin.fromlist(npz['origin'])
    f = common.FieldFromNPArr(npz['data'], mType=mType, sp=spacing, orig=origin)
    del npz.f
    npz.close()
    return f

def LoadImagePIL(fname, mType=core.MEM_HOST):
    if not HAS_PIL:
        raise Exception('Unable to load %s, PIL package not available'%fname)
    pilIm = Image.open(fname)
    npIm = np.array(pilIm, dtype=np.float32)
    I = common.ImFromNPArr(npIm, mType=mType)
    return I

def SaveImagePIL(Im, fname, outtype=np.uint8, rng=None):
    """
    Accepts 2D images in either numpy or PyCA format
    """

    if not HAS_PIL:
        raise Exception('Unable to save %s, PIL package not available'%fname)

    if isinstance(Im, core.Image3D):
        nparr = common.AsNPCopy(Im)
    elif isinstance(Im, np.ndarray):
        nparr = Im
    else:
        raise Exception('Unknown image object?')

    sz = list(np.atleast_3d(nparr).shape)

    if not issubclass(outtype, np.integer):
        raise Exception('floating point output not supported for this file type')

    info = np.iinfo(outtype)
    outrng = [info.min, info.max]

    if outtype is np.uint8:
        if sz[2] == 3:
            pilmode = 'RGB'
        elif sz[2] == 4:
            pilmode = 'RGBA'
        elif sz[2] == 1:
            pilmode='L'
        else:
            raise Exception('unsupported image dimension for file type: ' + str(sz))
    else:
        if sz[2] == 1:
            pilmode='I'
        else:
            raise Exception('unsupported image dimension for file type: ' + str(sz))
    if rng is None:
        rng = [None, None]

    assert len(rng) == 2
    if rng[0] is None:
        rng[0] = nparr.min()
    if rng[1] is None:
        rng[1] = nparr.max()

    nparr = (outrng[1]-outrng[0])*(nparr-rng[0])/(rng[1]-rng[0]) + outrng[0]

    nparr = nparr.astype(outtype)
    # pilIm = Image.fromarray(np.squeeze(nparr), mode=pilmode)
    pilIm = Image.fromarray(np.squeeze(nparr))
    pilIm.save(fname)


def LoadImageDCM(fname, mType=core.MEM_HOST):
    """Load a DICOM image in *.dcm format (with proper origin/spacing info)"""
    try:
        import dicom
    except:
        raise Exception('PyCA DICOM support requires pydicom, not found.')

    ds = dicom.read_file(fname)
    pixels = ds.pixel_array
    origin = core.Vec3Df()
    spacing = core.Vec3Df()
    if 'ImagePositionPatient' in ds.__dict__:
        origin.fromlist([ds.ImagePositionPatient[0], ds.ImagePositionPatient[1], 0])
    else:
        origin.fromlist([0,0,0])
    if 'PixelSpacing' in ds.__dict__:
        spacing.fromlist([ds.PixelSpacing[0], ds.PixelSpacing[1], 1])
    else:
        spacing.fromlist([1,1,1])

    return common.ImFromNPArr(pixels, mType=mType, sp=spacing, orig=origin)


def ReadHeader(fname):
    """
    Return data type (Image3D or Field3D) and grid for Image3D/Field3D
    stored in file fname
    """
    grid = core.GridInfo()
    dtype, ncomp = core._ITKFileIO.ReadHeader(grid, fname)
    if ncomp == 1:
        _type = core.Image3D
    elif ncomp == 3:
        _type = core.Field3D
    else:
        raise Exception('unknown type for data dimension ' + str(ncomp))
    return _type, grid


def SaveImage(Im, fname, useCompression=True, outtype=None, rng=None):
    """Save an image in any known format"""
    # get file extension
    ext = os.path.splitext(fname)[1].lower()

    # dispatch based on file extension
    if ext == '.npy':
        SaveImageNPY(Im, fname)
    elif ext == '.npz':
        SaveImageNPZ(Im, fname, useCompression=useCompression)
    elif ext in PIL_EXT:
        if outtype is None:
            outtype = np.uint8
        SaveImagePIL(Im, fname, outtype=outtype, rng=rng)
    elif ext == '.png':
        common.SavePNGImage(Im, fname, rng=rng)
    else:
        try:
            common.SaveITKImage(Im, fname, useCompression=useCompression)
        except IOError:
            raise Exception('File extension "'+ext+'" unknown.')


def LoadImage(fname, mType=core.MEM_HOST):
    """Load an image"""
    # get file extension
    ext = os.path.splitext(fname)[1].lower()

    # dispatch based on file extension
    if ext == '.npy':
        return LoadImageNPY(fname, mType=mType)
    elif ext == '.npz':
        return LoadImageNPZ(fname, mType=mType)
    elif ext in PIL_EXT:
        return LoadImagePIL(fname, mType=mType)
    elif ext == '.png':
        return common.LoadPNGImage(fname, mType=mType)
    elif ext == '.dcm':
        return LoadImageDCM(fname, mType=mType)
    else:
        try:
            return common.LoadITKImage(fname, mType=mType)
        except IOError:
            raise Exception('Could not load file.  Either the file extension "'
                +ext+'" is unknown or the file "'+fname+'" is missing or '
                +'corrupted.')


def SaveField(v, fname, useCompression=True):
    """Save a field in any known format"""
    # get file extension
    ext = os.path.splitext(fname)[1].lower()

    # dispatch based on file extension
    if ext == '.npy':
        raise Exception('Numpy field format not implemented')
    elif ext == '.npz':
        SaveFieldNPZ(v, fname, useCompression=useCompression)
    else:
        try:
            common.SaveITKField(v, fname, useCompression=useCompression)
        except IOError:
            raise Exception('File extension "'+ext+'" unknown.')


def LoadField(fname, mType=core.MEM_HOST):
    """Load a field in any known format"""
    # get file extension
    ext = os.path.splitext(fname)[1].lower()

    # dispatch based on file extension
    if ext == '.npy':
        raise Exception('Numpy field format not implemented')
    elif ext == '.npz':
        return LoadFieldNPZ(fname, mType=mType)
    else:
        try:
            return common.LoadITKField(fname, mType=mType)
        except IOError:
            raise Exception(('Could not load file.  Either the file extension "{0}" '
                             'is unknown or the file "{1}" is missing or '
                             'corrupted.').format(ext, fname))
