import PyCA.Core as core

import inspect

# Debugging, just add DebugHere() to start debugger at that line in
# ipython
try:
    from IPython.core.debugger import Tracer
except ImportError:
    ipy_debug_available = False
else:
    ipy_debug_available = True

def DebugHere():
    if ipy_debug_available:
        t = Tracer()
        t()
    else:
        print 'ipython not available for debugging'
        #raise Exception('ipython not available for debugging')

def GetOuterFrame():
    info = None
    # frame objects can create reference loops, make sure to manually
    # delete them
    try:
        curframe = inspect.currentframe()
        upframe = inspect.getouterframes(curframe)[2][0]
        info = inspect.getframeinfo(upframe)
    finally:
        del curframe
        del upframe

    return info[:3]


def CheckCUDAError(msg='', sync=True):
    (file, line, func) = GetOuterFrame()
    funcmsg = func + ": " + msg
    if sync:
        core.CheckCUDASynchError(file, line, funcmsg)
    else:
        core.CheckCUDAError(file, line, funcmsg)

def CUDAThreadSynchronize():
    core.CUDAThreadSynchronize()
