import numpy as np
# import scipy.sparse.linalg as splinalg
import scipy.sparse as sparse

import PyCA.Core as ca  # needed for background strategy constants

import Interp


def ImToColVec(im, order='F'):
    """
    Fortran-order flattening must be used with BilerpMatrix
    """
    colvec = im.flatten(order=order)
    colvec = colvec[:, None]
    return colvec


def ColVecToIm(colvec, sz, order='F'):
    """
    Fortran-order flattening must be used with BilerpMatrix
    """
    im = np.reshape(colvec, sz, order=order)
    return im


def SparseBilerpMat(harr, isDisplacement=False,
                    bc=ca.BACKGROUND_STRATEGY_CLAMP, sp=(1.0, 1.0, 1.0)):
    """
    Takes a 2D deformation harr (or displacement if
    isDisplacement is True).  Returns a sparse matrix representing
    deformation by bilinear interpolation, with clamped/wrapped boundary
    conditions
    """

    assert bc in [ca.BACKGROUND_STRATEGY_CLAMP, ca.BACKGROUND_STRATEGY_WRAP]

    sz = harr.shape
    #
    # Make sure this is a 2D deformation
    #
    if len(sz) == 3:
        if sz[2] != 2:
            raise Exception('Unable to interpret array of sz ' +
                            str(sz) + ' as a 2D deformtation field')
    elif len(sz) == 4:
        if sz[2] != 1 or not 2 <= sz[3] <= 3:
            raise Exception('Unable to interpret array of sz ' +
                            str(sz) + ' as a 2D deformtation field')
        harr = harr[:, :, 0, :2]
    else:
        raise Exception('Unable to interpret array of sz ' +
                        str(sz) + ' as a 2D deformtation field')
    sz = sz[:2]
    nVox = np.prod(sz)

    if isDisplacement:
        harr = Interp.VtoH(harr, sp=sp)

    hx = harr[:, :, 0].flatten(order='F')
    hy = harr[:, :, 1].flatten(order='F')

    assert len(hx) == nVox
    assert len(hy) == nVox

    # ixp and ixn are the previous and next integer indices, fx is
    # fractional position between them

    ixp = np.floor(hx)
    ixn = ixp + 1
    iyp = np.floor(hy)
    iyn = iyp + 1
    fx = hx - ixp
    fy = hy - iyp

    # handle out-of-bounds indices (ca.BACKGROUND_STRATEGY_WRAP only handles
    # one wrapping)
    for idxarr, dim in ((ixp, 0), (ixn, 0), (iyp, 1), (iyn, 1)):

        oob = idxarr < 0
        if bc == ca.BACKGROUND_STRATEGY_CLAMP:
            idxarr[oob] = 0.0
        elif bc == ca.BACKGROUND_STRATEGY_WRAP:
            idxarr[oob] += sz[dim]
        else:
            raise Exception('unknown bc')

        oob = idxarr > sz[dim] - 1
        if bc == ca.BACKGROUND_STRATEGY_CLAMP:
            idxarr[oob] = sz[dim] - 1
        elif bc == ca.BACKGROUND_STRATEGY_WRAP:
            idxarr[oob] -= sz[dim]
        else:
            raise Exception('unknown bc')

    # compute indices and values
    # p-previous, n-next

    idx_pp = iyp * sz[0] + ixp
    idx_pn = iyn * sz[0] + ixp
    idx_nn = iyn * sz[0] + ixn
    idx_np = iyp * sz[0] + ixn

    v_pp = (1.0 - fx) * (1.0 - fy)
    v_pn = (1.0 - fx) * fy
    v_nn = fx * fy
    v_np = fx * (1.0 - fy)

    # sanity check, make sure everything is properly clamped
    assert np.max(idx_pp) <= nVox - 1
    assert np.max(idx_pn) <= nVox - 1
    assert np.max(idx_nn) <= nVox - 1
    assert np.max(idx_np) <= nVox - 1
    assert np.min(v_pp) >= 0.0
    assert np.max(v_pp) <= 1.0
    assert np.min(v_pn) >= 0.0
    assert np.max(v_pn) <= 1.0
    assert np.min(v_nn) >= 0.0
    assert np.max(v_nn) <= 1.0
    assert np.min(v_np) >= 0.0
    assert np.max(v_np) <= 1.0

    # 4 entries per row
    data = [v_pp, v_pn, v_nn, v_np]
    data = [v[:, None] for v in data]
    data = np.hstack(data).flatten()

    indices = [idx_pp, idx_pn, idx_nn, idx_np]
    indices = [idx[:, None] for idx in indices]
    indices = np.hstack(indices).flatten()
    assert np.all(indices.astype(int) == indices)
    indices = indices.astype(int)

    indptr = np.arange(0, nVox * 4 + 1, 4)

    TM = sparse.csr_matrix((data, indices, indptr), shape=(nVox, nVox))

    return TM

if __name__ == '__main__':

    import PyCA.Common as common

    import matplotlib.pyplot as plt

    bc = ca.BACKGROUND_STRATEGY_CLAMP
    sz = [128, 120]
    sp = (1.2, 2.1, 1.0)
    imSp = ca.Vec3Df(sp[0], sp[1], sp[2])

    # image parameters
    # sqrSz=10
    nWaves = 0.5
    waveAmp = 4

    mType = ca.MEM_HOST

    # imArr = common.DrawChecker(sz, sqrSz)
    # imArr = common.DrawWavySinStripes(
    #     sz, nWaves=4, waveSz=20.0, nStripes=15)
    imArr = np.random.rand(*sz)

    im = common.ImFromNPArr(imArr, mType=mType, sp=imSp)

    v = common.WavyDef(sz,
                       nWaves=nWaves,
                       waveAmp=waveAmp,
                       waveDim=1,
                       mType=mType,
                       deformation=False)
    v.setSpacing(imSp)

    defIm = im.copy()
    ca.ApplyV(defIm, im, v, 1.0, bc)
    varr = common.AsNPCopy(v)

    IVec = ImToColVec(imArr)
    TM = SparseBilerpMat(harr=varr, isDisplacement=True, bc=bc, sp=sp)
    defArr = ColVecToIm(TM.dot(IVec), sz=sz)

    defImArr = np.squeeze(common.AsNPCopy(defIm))

    pw = 3
    ph = 1

    def pidx(r, c):
        return r * ph + c + 1

    plt.figure('Test of Trilerp Matrix')
    plt.clf()

    plt.subplot(ph, pw, pidx(0, 0))
    plt.title('matrix computed deformation')
    plt.imshow(defArr, cmap='gray')
    plt.colorbar()

    plt.subplot(ph, pw, pidx(0, 1))
    plt.title('PyCA computed deformation')
    plt.imshow(defImArr, cmap='gray')
    plt.colorbar()

    plt.subplot(ph, pw, pidx(0, 2))
    plt.title('difference')
    plt.imshow(defArr - defImArr, cmap='gray')
    plt.colorbar()

    plt.draw()
    plt.show()
